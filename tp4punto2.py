import os

def continuar():
    print()
    input('Presione una tecla para continuar...')
    os.system('cls')

def menu():
    print('1. Registrar productos: código, descripción, precio y stock.')
    print('2. Mostrar el listado de productos')
    print('3. Mostrar los productos cuyo stock se encuentre en el intervalo [A,B]')
    print('4. Diseñar un proceso que le sume X al stock de todos los productos cuyo valor actual de stock sea menor al valor Y.')
    print('5. Eliminar todos los productos cuyo stock sea igual a cero.')
    print('6. Salir')
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 6)):
        eleccion = int(input('Elija una opción: '))
    os.system('cls')
    return eleccion

# REGISTRAR PRODUCTOS

def registrarProductos():
    print('Cargar listado de productos...')
    productos = {}
    codigo = -1
    while (codigo != 0):
        codigo = int(input('Ingrese el código (coloque 0 para finalizar): '))
        if codigo != 0: 
            if codigo not in productos:    
                descripcion = input('Ingrese la descripción del producto: ')
                print('Precio: ')
                precio = leerNum()
                print('Stock: ')
                stock = leerNum()
                productos[codigo] = [descripcion,precio,stock]
                print('Se agregó el producto correctamente')
            else:
                print('El producto ya existe')
    return productos

# EL PRECIO Y EL STOCK NO PUEDEN SER NEGATIVOS
def leerNum():
    num= float(input('Ingrese el valor: '))
    while (num < 0):
        num= float(input('No se aceptan valores negativos, ingrese el valor nuevamente: '))
    return num

# MOSTRAR EL LISTADO DE PRODUCTOS

def mostrar(diccionario):
    print('Listado de productos')
    for clave, valor in diccionario.items():
        print(clave,valor)

# MOSTRAR PRODUCTOS EN UN INTERVALO

def productosIntervalo(productos):
    print('Lista de productos en el intervalo [A,B]: ')
    a= input('Ingrese el valor de A: ')
    b= input('Ingrese el valor de B: ')
    suma = 0
    for stock, datos in productos.items():
        if (datos[1] >= a) & (datos[1]<=b): 
            print(stock, datos)
            suma += datos[1]
        else:
            print ('No se encontro ningún producto en el intervalo ingresado') 

# SUMAR X AL STOCK MENOR A Y

def sumarX(productos):
    x= input('Ingrese el valor de X: ')
    y= input('Ingrese el valor de Y: ')
    suma = 0
    for stock, datos in productos.items():
        if (datos[1] < x): 
            stock = stock + y
            print(stock, datos)
            suma += datos[1]
        else:
            print ('No se encontro el valor ingresado')

# ELIMINAR PRODUCTOS CON STOCK IGUAL A CERO 

def buscarPorStock(productos):
    stock = int(input('Stock: '))
    datos = productos.get(stock,-1)
    return stock,datos

def eliminar(productos):
    print('Eliminar producto')
    stock, datos = buscarPorStock(productos)    
    if (datos != -1):
        print('Se va a eliminar: ', productos[stock])
        confirmacion = input('Confirma eliminarlo s/n: ')
        if (confirmacion == 's'):
            del productos[stock]
            print ('Eliminado ...')
    else:
        print('El stock no existe') 

# FIN DEL PROGRAMA

def salir():
    print('Fin del programa...')

# PRINCIPAL

opcion = 0
os.system('cls')
while (opcion != 6):
    opcion = menu()
    if opcion == 1:
        productos = registrarProductos()
    elif opcion == 2:
        mostrar(productos)
    elif opcion == 3:
        productosIntervalo(productos)
    elif opcion == 4:
        sumarX(productos)
    elif opcion == 5:
        eliminar(productos)
    elif opcion == 6:        
        salir()
    continuar()
